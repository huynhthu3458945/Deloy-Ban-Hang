﻿function ChangeCulture(language) {
    var originalUrl = window.location;
    var redirectUrl = '/_common-settings/ChangeCulture?culture=' + language + '&url=' + originalUrl;
    window.location = redirectUrl;
}