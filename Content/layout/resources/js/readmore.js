var outerWidth = window.innerWidth;

function readmore(item,min){    
  var limit,lm,check = false;  
  limit = Object.keys(min).sort((a,b)=>parseInt(a)-parseInt(b));

  limit.forEach(function(item,id){
    if(outerWidth >= parseInt(item)){
      lm = min[item];
    }
    if(id == limit.length - 1){
      check = true;
    }
  });
  
  const li = document.createElement("li");
  li.innerHTML = item;      
  const txt = li.innerText.length > lm ? li.innerText.slice(0,lm)+"..." : li.innerText;    

  return txt; 
}
const _limittextlines = function(){
  const stringblog= $('.txt_art h4');
  if(stringblog) {
    for( i = 0 ; i < stringblog.length ; i ++){
      var textnameblog = readmore(stringblog[i].textContent,{'320':55});
      stringblog[i].innerHTML = textnameblog;
    }    
  }
  
  const stringblogscript= $('.txt_art p');
  if(stringblogscript) {
    for( i = 0 ; i < stringblogscript.length ; i ++){
      var textscriptblog = readmore(stringblogscript[i].textContent,{'320':110});
      stringblogscript[i].innerHTML = textscriptblog;
      console.log(textscriptblog);
    }
  }

  const stringtxt = $('.ct_itpd h3');
  if(stringtxt) {
    for( i = 0 ; i < stringtxt.length ; i ++){
      var textname = readmore(stringtxt[i].textContent,{'320':56});
      stringtxt[i].innerHTML = textname;
      console.log(textname);
    }
  }

  
  const subblogpostname = $('.sub-blog_post .blog-title a');
  if(subblogpostname) {
    for( i = 0 ; i < subblogpostname.length ; i ++){
      var textscriptsubblog = readmore(subblogpostname[i].textContent,{'320':62});
      console.log(textscriptsubblog);
      subblogpostname[i].innerHTML = textscriptsubblog;
    }
  }

  const subarticlepostname = $('.sub-blog_post .blog-article');
  if(subarticlepostname) {
    for( i = 0 ; i < subarticlepostname.length ; i ++){
      var textscriptsubblog = readmore(subarticlepostname[i].textContent,{'320':120});
      console.log(textscriptsubblog)
      subarticlepostname[i].innerHTML = textscriptsubblog;
    }
  }

  const blogarticleboxname = $('.blog-article-box .blog-title a');
  if(!blogarticleboxname) {
    for( i = 0 ; i < blogarticleboxname.length ; i ++){
      var textscriptsubblog = readmore(blogarticleboxname[i].textContent,{'320':88});
      console.log(textscriptsubblog)
      blogarticleboxname[i].innerHTML = textscriptsubblog;
    }
  }

  const blogarticletxt = $('.blog-article');
  if(!blogarticletxt) {
    for( i = 0 ; i < blogarticletxt.length ; i ++){
      var textscriptsubblog = readmore(blogarticletxt[i].textContent,{'320':380});
      console.log(textscriptsubblog)
      blogarticletxt[i].innerHTML = textscriptsubblog;
    }
  }
  const blogdtaillistitemtitle = $('.blog-detail__list-item__title');
  if(!blogdtaillistitemtitle) {
    for( i = 0 ; i < blogdtaillistitemtitle.length ; i ++){
      var txtblogdtaillistitemtt = readmore(blogdtaillistitemtitle[i].textContent,{'320':380});
      console.log(blogdtaillistitemtt)
      blogdtaillistitemtitle[i].innerHTML = txtblogdtaillistitemtt;
    }
  }
  
}
_limittextlines();

$(window).resize(function(){
  _limittextlines();
});
window.onload = function () {
 document.addEventListener("contextmenu", function (e) {
     e.preventDefault();
 }, false);
 document.addEventListener("keydown", function (e) {
     if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
         disabledEvent(e);
     }
     if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
         disabledEvent(e);
     }
     if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
         disabledEvent(e);
     }
     if (e.ctrlKey && e.keyCode == 85) {
         disabledEvent(e);
     }
     if (event.keyCode == 123) {
         disabledEvent(e);
     }
 }, false);
 function disabledEvent(e) {
     if (e.stopPropagation) {
         e.stopPropagation();
     } else if (window.event) {
         window.event.cancelBubble = true;
     }
     e.preventDefault();
     return false;
 }
};