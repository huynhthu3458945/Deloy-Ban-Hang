﻿
// Add products to cart
function AddToCart(productId) {
    $.ajax({
        method: "POST",
        url: "/ShoppingCart/AddToCart",
        data: { productId: productId }
    }).then(function (data) {
        ShowToastOrder("Đã thêm vào giỏ hàng");
        //_showdialog("add_to_cart_success");

        // Update Header Cart Information
        $(".cart-count").html(data.totalCartItems);
    });
}

function ShowToastOrder(responseText) {
    $.toast({
        heading: 'Đơn hàng',
        text: responseText,
        position: 'top-right',
        loaderBg: '#9EC600',
        icon: 'success'
    })
}

function AddToCartDetail(productId) {
    setTimeout(function () {
        var itemCount = parseInt($("#item_count").val());

        $.ajax({ method: "POST", url: "/ShoppingCart/AddToCart", data: { productId: productId, itemCount: itemCount } })
            .then(function (data) {
                //_showdialog("add_to_cart_success");
                // Update Header Cart Information
                ShowToastOrder("Đã thêm vào giỏ hàng");
                $(".cart-count").text(data.totalCartItems);
            });
    })
}

// Update itemcount in ShoppingCart Page
function UpdateItemCount(tokenCode, rowId) {
    $('.amount input').on('input', function () {
        if ($(this).val() <= 0) {
            $(this).val(1);
            UpdateItemCount(tokenCode, rowId);
        }
    });

    setTimeout(function () {
        // Get item price
        var price = $("#price" + rowId).data('price');

        // Get current item count
        var itemCount = parseInt($("#itemCount" + rowId).val());

        // Update ShoppingCartItems
        UpdateShoppingCartItems(tokenCode, itemCount);

        // Update item amount
        var amount = price * itemCount;
        $("#amount" + rowId).text(amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " VND");

        // Update total amount
        var oldTotal = $("#old_total").val();
        var oldItemCount = $("#oldItemCount" + rowId).val();
        var total = parseInt(oldTotal) + (itemCount - oldItemCount) * price;
        var totalStr = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' vnđ';
        $("#total").text(totalStr);
        $("#old_total").val(total);
        $("#oldItemCount" + rowId).val(itemCount);
    })

}

function UpdateShoppingCartItems(tokenCode, itemCount) {
    $.ajax({ method: "POST", url: "/ShoppingCart/UpdateShoppingCartItems", data: { tokenCode: tokenCode, itemCount: itemCount } }).done(function (data) {
        // Update Header Cart Information
        $(".cart-count").text(data.totalCartItems);
    });
}

// Delete shoppingCartItems in ShoppingCart Page
function DeleteCartItem(tokenCode, rowId) {
    $.ajax({ method: "POST", url: "/ShoppingCart/DeleteShoppingCartItems", data: { tokenCode: tokenCode } }).done(function (data) {
        var totalStr = data.totalAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' VNĐ';
        ShowToastOrder("Xóa thành công.")
        // Update total amount
        $("#total").text(totalStr);
        $("#old_total").val(data.totalAmount);
        // Remove current row
        var id = "#" + (rowId); $(id).remove();
        // Update Header Cart Information
        $(".cart-count").text(data.totalCartItems);
        //UpdateStyleOfCart();
    });
}


function CheckOut() {
    let name = $("#Name").val();
    let email = $("#Email").val();
    let phone = $("#Phone").val();
    let inpAddress = $("#inpAddress").val();
    if (name == "" || name == null) {
        alert("Vui lòng nhập họ và tên");
        return;
    }
    if (phone == "" || phone == null) {
        alert("Vui lòng nhập số điện thoại");
        return;
    }
    if (email == "" || email == null) {
        alert("Vui lòng nhập email");
        return;
    }  
    if (inpAddress == "" || inpAddress == null) {
        alert("Vui lòng nhập địa chỉ");
        return;
    } 
    let selectCity = $("#selectCity option:selected").text();
    let selectDistrict = $('#selectDistrict option:selected').text();   
    let address = inpAddress + "," + selectDistrict + "," + selectCity;
    var model = {};
    model.Name = $("#Name").val();
    model.Email = $("#Email").val();
    model.Phone = $("#Phone").val();
    model.Address = address;
    model.OrderTypeId = $('input[name=OrderTypeId]:checked', '#OrderCheckOut').val();
    model.Note = $("#Note").val();
    $.ajax({
        method: "POST",
        url: "/Order/Checkout",
        data: { model: model }
    }).then(function (data) {
        ShowToastOrder("Đặt thành công.Cám ơn quý khách đã đặt hàng.")      
        window.location.href = "/";
        $(".cart-count").html(0);
    });
}
