if ($('.content').hasClass('funnel-success')){  
	$(".fc_modal-backdrop, .fc-form-modal").remove();
	$("body").removeClass('fc_modal-open');
}

$(document).ready(function(){
	$(window).bind("load", function() {

		// LuckyDraw Show/Hide Info
		if ($('body').hasClass('luckydraw-page')){  
			$(".content-view").addClass('less-view');
			$(".show-hide").on("click", function () {
					var txt = $(".content-view").hasClass('less-view') ? 'Rút gọn' : 'Xem đầy đủ';
					$(".show-hide").text(txt);
					$('.content-view').toggleClass('less-view');
			});
		}


		// elevateZoom
		if ($(window).width() >= 768 && $('body').hasClass('single-product')){  
			$(".img-zoom").elevateZoom({zoomWindowFadeIn: 500, zoomWindowFadeOut: 500,	lensFadeIn: 500, lensFadeOut: 500, zoomWindowWidth: 430, zoomWindowHeight: 545, borderSize: 0});
			// $(".img-zoom").elevateZoom({zoomType: "inner", cursor: "crosshair"});
			// $(".img-zoom").bind("click", function(e) {  
			//   var ez =   $('.img-zoom').data('elevateZoom');	
			// 	$.fancybox(ez.getGalleryList());
			//   return false;
			// });
		}

		// Hide like num
		// console.log("12");
    // $('iframe').contents().find("head").append($("<style type='text/css'>  ._1drq, ._1drm._1ds3 ._1drq {display:none!important;}  </style>"));


		// Ekko Lightbox
		$(document).on('click', '[data-toggle="lightbox"]', function(event) {
			event.preventDefault();
			$(this).ekkoLightbox({
				alwaysShowClose: true
				// showArrows: true,
			});
		});

		// Scroll-Then-Fix Top header  
		// if ($(window).width() >= 991) {
			$(window).scroll(function() {
				if ($(this).scrollTop() > 147 && ($('body').hasClass('product-detail-page') || $('.content').hasClass('sale-page'))){  
					$('.top-cta').show();
					// Scroll-Then-Fix sidebar 
					// var elementPosition = $('.product-info-detail').offset();
					// var relate = $(".product-box").offset().top;
					// var sidebar_box = $('.sidebar-box').height();
					// if ($(window).scrollTop() > elementPosition.top-90 && $(window).scrollTop()<relate-sidebar_box-100){ 
					// 	$('.sidebar').addClass('sidebar-sticky');
					// }
					// else{
					// 	$('.sidebar').removeClass('sidebar-sticky');
					// }
				}
				else{
					$('.top-cta').hide();
				}
			}); 	
		// } 


		

		if ($('.content').hasClass('product-category-page')){  
			function promoDate(num) {
				var newDt= new Date();
				newDt.setDate(newDt.getDate() + num);

				var month = newDt.getMonth()+1;
				var day = newDt.getDate();

				var output = ((''+day).length<2 ? '0' : '') + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' +newDt.getFullYear();
				return output;
			}

			$('.promo-date').html(promoDate(7));
		}



		// Click on navbar-toggler button
		// var mainNavbar = $('.navbar-toggler');
		// var mainNavbarContainer = $('.main-nav');
		// var maxHeight = $(window).height() - $('.header').height() + 1;

		// mainNavbar.click(function(){
		// 	// console.log(maxHeight);
		// 	if ($(this).hasClass('collapsed')){ 
		// 		$(mainNavbarContainer).height(maxHeight);
		// 		$('body').addClass('mobile-menu-sticky');
		// 	}
		// 	else{
		// 		$(mainNavbarContainer).height(0);
		// 		$('body').removeClass('mobile-menu-sticky');
		// 	}
			
		// });

		// scrollUp
		var scrollButton = $('#scrollUp');
		var mobileFooterNavbar = $('.mobile-footer-navbar');
		var bottomFuntion = $('.bottom-function');
		$(window).scroll(function(){
			$(this).scrollTop() >= 1000 ? bottomFuntion.show() : bottomFuntion.hide();
			$(this).scrollTop() >= 1000 ? mobileFooterNavbar.addClass('active') : mobileFooterNavbar.removeClass('active');			
			// console.log($(this).scrollTop());
		});
		
		// Click on scrollUp button
		scrollButton.click(function(){
			// console.log("click");
			$('html,body').animate({scrollTop:0},600);
			
		});

		$('#contentAccordion').on("show.bs.collapse", function () {
				 $('.collapse.in').collapse('hide');
				 console.log("aâ")
		});

		// Fix main-nav
		// if ($(window).width() >= 768) {
		// 	$('ul.nav li.dropdown').hover(function() {
		// 		$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
		// 	}, function() {
		// 		$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
		// 	});			
		// }

		if ($(window).width() >= 991){ 
		var placeholderText = [
		"Tổ yến chưa chế biến ?",
		"Tổ yến thô ?",
		"Nước yến ?",
		];
		$('#placeholder-type-writter').placeholderTypewriter({text: placeholderText});
		}

		// Auto pop-up promoModal
		// $('#promoModal').modal('show')

		// Enable tooltips everywhere
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		})


		// Click on Voucher-btn
		if ($('body').hasClass('page-template-cart')){
			$(".voucher-box .btn-link").click(function(event) {
				console.log("acs");
				$(".voucher-box p").addClass('hidden')
				$(".voucher-form").removeClass('hidden');
			});
		}

		

	});
});